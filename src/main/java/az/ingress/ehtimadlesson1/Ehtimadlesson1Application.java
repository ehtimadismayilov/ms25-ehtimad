package az.ingress.ehtimadlesson1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ehtimadlesson1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ehtimadlesson1Application.class, args);
	}

}
